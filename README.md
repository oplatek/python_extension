Intro
-----
The snippets show how to build an C extensions
using `Swig2.04` and `distutils`.

The sample extension computes factorial function.
If the build is successful than the `fact` function can be used from Python.
Consequently the unittest should be successful:
```bash
python test.py
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
```

Instructions
------------

Install latest swig2.04 by `sudo apt-get install swig2.0`
run the example from [http://sebsauvage.net/python/mingw.html]{http://sebsauvage.net/python/mingw.html}

If you run into missing  `Python.h` -> install Python headers by `sudo apt-get install python2.7-dev`


```bash
python setup.py build_ext --inplace  # located in build.sh
```


You can try more complicated attitude which also works by running `setup2.py` and swig separately.

```bash
# The command are stored in build2.sh
swig -python example.i
python setup2.py build_ext --inplace  # note the digit 2!
```

Links
-----
 * [Distutils tutorial](http://docs.python.org/distutils/setupscript.html)
 * [Swig documentation](http://www.swig.org/Doc1.3/Python.html#Python_nn12)
