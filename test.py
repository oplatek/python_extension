#!/usr/bin/env python

import unittest
from itertools import izip


class TestBuild(unittest.TestCase):
    def setUp(self):
        import example
        self.f = example.fact

    def testFact(self):
        xs = range(10)
        fxs = [self.f(x) for x in xs]
        results = [1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]
        for (fx, r) in izip(fxs, results):
            self.assertEqual(fx, r)


if __name__ == '__main__':
    unittest.main()
